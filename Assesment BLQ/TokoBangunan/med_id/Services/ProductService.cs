﻿using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ProductService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMTblProduct>> GetAllData()
        {
            List<VMTblProduct> data = new List<VMTblProduct>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProduct/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiResponse)!;

            return data;
        }

        public async Task<bool> CheckByName(string name, int id, int idVariant)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/CheckByName/{name}/{id}/{idVariant}");
            bool isexist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isexist;
        }


        public async Task<VMResponse> Create(VMTblProduct dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteAPI + "apiProduct/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMTblProduct> GetDataById(int id)
        {
            VMTblProduct data = new VMTblProduct();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProduct>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(VMTblProduct dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PutAsync(RouteAPI + "apiProduct/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiProduct/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
