﻿//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();
//Nomor8();
//Nomor9();
//Nomor10();
//Nomor11();
//Nomor12();
//Nomor13();
//Nomor14();
//Nomor15();
//Nomor16();
//Nomor17();
//Nomor18();
//Nomor19();
//Nomor20();
Nomor21();
//Nomor22();
Console.ReadKey();

static void Nomor1()
{
    int[] kacamata = { 500, 600, 700, 800 };
    int[] baju = { 200, 400, 350 };
    int[] sepatu = { 400, 350, 200, 300 };
    int[] buku = { 100, 50, 150 };

    int budget = 1000;
    int bill = 0;
    int selPrev = 0;
    int selisihbudget = 0;


    List<string> belanja = new List<string>();

    for (int i = 0; i < kacamata.Length; i++)
    {
        for (int j = 0; j < baju.Length; j++)
        {
            for (int k = 0; k < sepatu.Length; k++)
            {
                for (int l = 0; l < buku.Length; l++)
                {
                    bill = kacamata[i] + baju[j] + sepatu[k] + buku[l];
                    selisihbudget = budget - bill;
                    if (bill <= 1000 && selisihbudget <= selPrev)
                    {
                        belanja.Clear();
                        belanja.Add($"kacamata {kacamata[i]}");
                        belanja.Add($"baju {baju[j]}");
                        belanja.Add($"sepatu {sepatu[k]}");
                        belanja.Add($"buku {buku[l]}");

                        selPrev = selisihbudget;
                    }


                }
            }
        }
    }

    Console.WriteLine($"\nJumlah item : {belanja.Count()} ({String.Join(", ", belanja)})");

}


static void Nomor2()
{
    int a = 14;
    int b = 3;
    int c = 7;
    int d = 7;

    int denda = 100;
    int totalDenda = 0;

    int cek;

    Console.WriteLine("== Parsing Date Time ==");
    Console.Write("Masukan Peminjaman (dd/MM/yyyy) : ");
    string dateString = Console.ReadLine();
    Console.Write("Masukan Pengembalian (dd/MM/yyyy) : ");
    string dateString1 = Console.ReadLine();

    DateTime date1 = DateTime.ParseExact(dateString, "d/M/yyyy", null);
    DateTime date2 = DateTime.ParseExact(dateString1, "d/M/yyyy", null);
    TimeSpan interval = date2 - date1;
    int inDays = interval.Days;


    cek = inDays - a;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = inDays - b;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = inDays - c;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = inDays - d;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }



    Console.WriteLine("Jumlah Denda : " + totalDenda);
}

static void Nomor3()
{
    double totalTarif = 0;
    Console.WriteLine("== Parsing Date Time ==");
    Console.Write("Waktu Masuk (dd/MM/yyyy|HH/mm/ss) : ");
    string dateString = Console.ReadLine();
    Console.Write("Waktu Keluar (dd/MM/yyyy) : ");
    string dateString1 = Console.ReadLine();

    DateTime date1 = DateTime.ParseExact(dateString, "d/M/yyyy|HH/mm/ss", null);
    DateTime date2 = DateTime.ParseExact(dateString1, "d/M/yyyy|H/m/s", null);
    TimeSpan interval = date2 - date1;
    double inDays = interval.TotalSeconds;
    double jam = inDays / 3600;
    double hari = jam / 24;
    if (hari <= 1)
    {
        if (jam <= 8)
        {
            totalTarif += Math.Ceiling(jam) * 1000;
        }
        else
        {
            totalTarif = 8000;
        }
    }
    else
    {
        double sisaJam = Math.Ceiling(jam - 24) * 1000;
        totalTarif = 15000 + sisaJam;
    }

    Console.WriteLine("Jumlah Tarif : " + totalTarif);
}

static void Nomor4()
{
    Console.Write("Masukkan jumlah bilangan prima pertama yang ingin anda tampilkan: ");
    int n = Convert.ToInt32(Console.ReadLine());

    int count = 0;
    int num = 2;
    while (count < n)
    {
        bool isPrime = true;
        if (num <= 1) isPrime = false;
        if (num == 2) isPrime = true;
        if (num % 2 == 0) isPrime = false;
        for (int i = 3; i * i <= num; i += 2)
        {
            if (num % i == 0) isPrime = false;
        }
        if (isPrime)
        {
            Console.Write(num + " ");
            count++;
        }
        num++;
    }
}

static void Nomor5()
{
    Console.Write("Masukkan jumlah bilangan Fibonacci pertama yang ingin anda tampilkan: ");
    int n = Convert.ToInt32(Console.ReadLine());

    int a = 0;
    int b = 1;
    int count = 0;
    while (count < n)
    {
        Console.Write(a + " ");
        int temp = a;
        a = b;
        b = temp + b;
        count++;
    }
}

static void Nomor6()
{
    Console.WriteLine("Masukkan kata: ");
    string kata = Console.ReadLine();

    int panjang = kata.Length;
    bool tmp = false;

    for (int i = 0; i < panjang / 2; i++)
    {
        if (kata[i] != kata[panjang - 1 - i])
        {
            tmp = false;
        }
        else
        {
            tmp = true;

        }
    }

    if (tmp == true)
    {
        Console.Write($"{kata} adalah palindrome.");
    }
    else
    {
        Console.Write($"{kata} bukan palindrome.");
    }
}

static void Nomor7()
{
    Console.Write("masukan angka = ");

    double[] array = Array.ConvertAll(Console.ReadLine().Split(' ', ','), Convert.ToDouble);

    double mean;
    double tam = 0;
    double a;
    double modus = array.Max();
    foreach (double item in array)
    {
        tam += item;
    }

    mean = tam / array.Length;

    Console.WriteLine("Nilai Mean = " + mean);

    for (int i = 0; i < array.Length; i++)
    {
        for (int j = i + 1; j < array.Length; j++)
        {
            if (array[j] < array[i])
            {
                a = array[i];
                array[i] = array[j];
                array[j] = a;
            }
        }
    }

    int tes = array.Length / 2;
    int cek = array.Length % 2;

    if (cek == 0)
    {
        Console.WriteLine("Nilai Median = " + array[tes] + " dan " + array[tes - 1]);
    }
    else
    {
        Console.WriteLine("Nilai Median = " + array[tes]);
    }

    int banding = 0;
    for (int i = 0; i < array.Length;)
    {
        int nah = 1;
        for (int j = i + 1; j < array.Length; j++)
        {
            if (array[j] == array[i])
            {
                nah++;
            }
        }
        if (nah > banding)
        {
            modus = array[i];
            banding = nah;
        }
        else if (nah == banding && modus > array[i])
        {
            modus = array[i];
        }

        i += nah;

    }
    Console.WriteLine("Nilai Modus = " + modus);
}

static void Nomor8()
{
    List<int> list = new List<int>() { 1, 2, 4, 7, 8, 6, 9 };

    List<int> maxList = list.GetRange(0, list.Count);
    int totalmax = 0;

    List<int> minList = list.GetRange(0, list.Count);
    int totalmin = 0;

    for (int i = 0; i < 4; i++)
    {
        int max = 0;
        for (int j = 0; j < maxList.Count; j++)
        {
            if (maxList[j] > max)
            {
                max = maxList[j];
            }
        }
        totalmax += max;
        maxList.Remove(max);

        int min = int.MaxValue;
        for (int j = 0; j < minList.Count; j++)
        {
            if (minList[j] < min)
            {
                min = minList[j];
            }
        }
        totalmin += min;
        minList.Remove(min);
    }
    Console.WriteLine(totalmax);
    Console.WriteLine(totalmin);

}

static void Nomor9()
{
    Console.Write("Masukkan nilai N: ");
    int n = int.Parse(Console.ReadLine());
    for (int i = 1; i <= n; i++)
    {
        int hasil = i * n;
        Console.Write($"{hasil}");

        if (i < n)
        {
            Console.Write(", ");
        }
    }
    Console.WriteLine();
}

static void Nomor10()
{
    Console.Write("masukan Kalimat = ");
    string[] n = Console.ReadLine().Split(' ');

    for (int i = 0; i < n.Length; i++)
    {
        Console.Write(n[i].Substring(0, 1) + "***" + n[i].Substring(n[i].Length - 1, 1) + " ");
    }

}

static void Nomor11()
{
    Console.Write("masukan Kata = ");
    string n = Console.ReadLine();

    for (int i = n.Length - 1; i >= 0; i--)
    {
        Console.WriteLine("***" + n.Substring(i, 1) + "***" + " ");
    }
}

static void Nomor12()
{
    Console.Write("masukan angka = ");

    double[] array = Array.ConvertAll(Console.ReadLine().Split(' ', ','), Convert.ToDouble);


    double a;

    for (int i = 0; i < array.Length; i++)
    {
        for (int j = i + 1; j < array.Length; j++)
        {
            if (array[j] < array[i])
            {
                a = array[i];
                array[i] = array[j];
                array[j] = a;
            }
        }
    }
    foreach (double item in array)
    {
        Console.Write(item + " ");
    }
}

static void Nomor13()
{
    Console.Write("masukan Jam H:mm = ");
    string jam = Console.ReadLine();
    int[] array = Array.ConvertAll(jam.Split(':'), int.Parse);

    int x = array[0] * 30;
    int y = array[1] * 6;

    int hasil = Math.Abs(x - y);

    if (hasil > 180)
    {
        hasil = 360 - hasil;
    }
    Console.WriteLine("Jam " + jam + " -> " + hasil);
}

static void Nomor14()
{
    Console.Write("masukan Deret = ");
    string[] deret = Console.ReadLine().Split(' ');
    Console.Write("masukan n = ");
    int n = int.Parse(Console.ReadLine());

    string nih = "";

    for (int i = 0; i < deret.Length; i++)
    {
        nih += deret[i];
    }

    nih = nih.Substring(n) + nih.Substring(0, n);

    foreach (var item in nih)
    {
        Console.Write(item + " ");
    }
}


static void Nomor15()
{
    Console.Write("Masukkan waktu dalam format 'hh:mm:ss tt': ");
    string waktuString = Console.ReadLine();

    DateTime waktu;

    if (DateTime.TryParseExact(waktuString, "hh:mm:ss tt", null, System.Globalization.DateTimeStyles.None, out waktu))
    {
        string waktu24Jam = waktu.ToString("HH:mm:ss");

        Console.WriteLine("Waktu dalam format 24 jam: " + waktu24Jam);
    }
    else
    {
        Console.WriteLine("Format waktu tidak valid. Pastikan menggunakan format 'hh:mm:ss tt'.");
    }

}

static void Nomor16()
{
    List<string> list = new List<string>();
    List<char> ikanlist = new List<char>();
    List<double> dou = new List<double>();

cek:
    Console.Write("masukan Jenis Makanan = ");
    string kata = Console.ReadLine();
    list.Add(kata);

    Console.Write("Mengandung Ikan ? y/t = ");
    char ik = Convert.ToChar(Console.ReadLine());
    ikanlist.Add(ik);

    Console.Write("masukan harga = ");
    double harga = Convert.ToDouble(Console.ReadLine());
    dou.Add(harga);

    Console.Write("Tambah Makanan ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();

    if (jawab == "ya")
    {
        goto cek;
    }

    double a = 0;
    double b = 0;

    for (int i = 0; i < list.Count; i++)
    {
        if (ikanlist[i] == 'y')
        {
            a += dou[i];
        }
        else
        {
            b += dou[i];
        }
    }

    double q = a * 10 / 100;
    double w = a * 5 / 100;
    double e = b * 10 / 100;
    double r = b * 5 / 100;

    double p = (a + b) * 10 / 100;
    double s = (a + b) * 5 / 100;
    double total = a + b;
    double bayar = a + b + p + s;

    double ptpt = (b + e + r) / 4;
    double pt = ptpt + ((a + q + w) / 3);

    for (int i = 0; i < list.Count; i++)
    {
        if (ikanlist[i] == 'y')
        {
            Console.WriteLine($"{list[i]} *ikan {dou[i]}");
        }
        else
        {
            Console.WriteLine($"{list[i]} {dou[i]}");
        }
    }
    Console.WriteLine("Total Harga Pesanan = " + total);
    Console.WriteLine("Pajak 10% = " + p);
    Console.WriteLine("Servis 5% = " + s);
    Console.WriteLine("Total Pembayaran = " + bayar);
    Console.WriteLine("Patungan 3 Orang yang tidak alergi = " + pt);
    Console.WriteLine("Patungan 1 Orang yang alergi = " + ptpt);
}
static void Nomor17()
{
    Console.Write("Masukkan N/T = ");
    string[] x = Console.ReadLine().Split(' ');

    int gunung = 0;
    int lembah = 0;

    for (int i = 0; i < x.Length - 1; i++)
    {
        if (x[i] == "N" && x[i + 1] == "T")
        {
            gunung++;
        }
        if (x[i] == "T" && x[i + 1] == "N")
        {
            lembah++;
        }
    }

    Console.WriteLine($"Gunung {gunung} dan Lembah {lembah}");
}
static void Nomor18()
{
    List<double> kalori = new List<double>();
    List<double> jam = new List<double>();

cek:

    Console.Write("Masukan Jam = ");
    double ik = Convert.ToDouble(Console.ReadLine());
    jam.Add(ik);

    Console.Write("Jumlah Kalori = ");
    double kal = Convert.ToDouble(Console.ReadLine());
    kalori.Add(kal);

    Console.Write("Tambah makan kue ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();



    if (jawab == "ya")
    {
        goto cek;
    }
    Console.Write("Olahraga Jam = ");
    double ol = Convert.ToDouble(Console.ReadLine());

    Console.WriteLine("    Jam       Kalori");

    for (int i = 0; i < jam.Count; i++)
    {
        if (jam[i] < 10)
        {
            Console.WriteLine("      " + jam[i] + "        " + kalori[i]);
        }
        else
        {
            Console.WriteLine("     " + jam[i] + "        " + kalori[i]);
        }

    }

    double waktu = 0;
    double j, w;

    for (int i = 0; i < jam.Count; i++)
    {
        j = ol - jam[i];
        w = 0.1 * kalori[i] * (j * 60);
        waktu += w;
    }
    int nah = Convert.ToInt32(waktu);
    int minum = ((nah / 30) * 100) + 500;

    Console.WriteLine("Perlu Minum Sebanyak " + minum + " cc");
}

static void Nomor19()
{
    Console.Write("Masukkan kalimat: ");
    string inputKalimat = Console.ReadLine();

    string cleaned = new string(inputKalimat.Where(char.IsLetter).Select(char.ToLower).ToArray());

    bool isPangram = cleaned.Distinct().Count() == 26;

    if (isPangram)
    {
        Console.WriteLine("Kalimat tersebut adalah pangram.");
    }
    else
    {
        Console.WriteLine("Kalimat tersebut bukan pangram.");
    }
}

static void Nomor20()
{
    List<string> a = new List<string>();
    List<string> b = new List<string>();

cek:

    Console.Write("Suit A = ");
    string sa = Console.ReadLine().ToLower();
    a.Add(sa);

    Console.Write("Suit B = ");
    string sb = Console.ReadLine().ToLower();
    b.Add(sb);

    Console.Write("Suit Lagi ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();



    if (jawab == "ya")
    {
        goto cek;
    }

    Console.Write("Jarak Awal = ");
    int jarak = int.Parse(Console.ReadLine());

    int pa = 0; int pb = jarak;

    string win = "";

    for (int i = 0; i < a.Count; i++)
    {
        string ceka = a[i].ToLower();
        string cekb = b[i].ToLower();


        if (ceka == "g" && cekb == "b")
        {
            pb = pb + 1;
            win = "B";
        }
        if (ceka == "g" && cekb == "k")
        {
            pa = pa + 1;
            win = "A";
        }

        if (ceka == "b" && cekb == "k")
        {
            pb = pb + 1;
            win = "B";
        }
        if (ceka == "b" && cekb == "g")
        {
            pa = pa + 1;
            win = "A";
        }

        if (ceka == "k" && cekb == "b")
        {
            pa = pa + 1;
            win = "A";
        }
        if (ceka == "k" && cekb == "g")
        {
            pb = pb + 1;
            win = "B";
        }

        if (pa == pb)
        {
            Console.WriteLine("Yang Menang = " + win);
            break;
        }
    }
}

static void Nomor21()
{
    List<int> STList = new List<int>();
    List<int> DList = new List<int>();

    Console.Write("Masukkan panjang lintasan: ");
    int lintasan = int.Parse(Console.ReadLine());

    Console.Write("Masukkan index lubang: ");
    int lubang = int.Parse(Console.ReadLine());

    for (int i = 0; i < lintasan; i++)
    {
        STList.Add(0);
        DList.Add(0);
    }

    Console.WriteLine("Contoh lintasan:");
    for (int i = 0; i < lintasan; i++)
    {
        if (i == 0)
            Console.Write("T ");
        else if (i == lubang)
            Console.Write("O ");
        else
            Console.Write("_ ");
    }
    Console.WriteLine("Finish");

    int ST = 0, D = 0;
    int steps = 0;

    while (D <= lintasan - 1)
    {
        Console.Write("Jalan/Lompat? = ");
        string aksi = Console.ReadLine().ToLower();

        if (aksi == "jalan")
        {
            STList[D] += 1;
            DList[D] += 1;
            ST += 1;
            D += 1;
        }
        else if (aksi == "lompat")
        {
            if (ST >= 2 && D < lintasan - 2)
            {
                STList[D] -= 2;
                DList[D] += 3;
                ST -= 2;
                D += 3;
            }
        }
        else
        {
            Console.WriteLine("Perintah tidak valid!");
        }

        steps++;

        Console.WriteLine("Langkah ke-{0}:", steps);
        for (int i = 0; i < lintasan; i++)
        {
            if (i == D)
                Console.Write("T ");
            else if (i == lubang)
                Console.Write("O ");
            else
                Console.Write("_ ");
        }
        Console.WriteLine("Finish");

        if ( D == lubang || D == lubang - 1 && ST < 2)
        {
            Console.WriteLine("KAMU KALAH!");
            Console.WriteLine("KAMU KEKURANGAN STAMINA!");
            Console.WriteLine("KAMU TERKENA LUBANG!");
            break;
        }
    }

    if (D >= lintasan - 1)
    {
        Console.WriteLine($"Selamat! Anda berhasil mencapai finish dalam {steps} langkah.");
        Console.WriteLine("Langkah yang diambil:");

        for (int i = 0; i < steps; i++)
        {
            if (STList[i] > 0)
            {
                for (int j = 0; j < STList[i]; j++)
                {
                    Console.Write("W ");
                }
            }
            else
            {
                for (int j = 0; j < Math.Abs(STList[i]); j++)
                {
                    Console.Write("J ");
                }
            }
        }
        Console.WriteLine();
    }
}

static void Nomor22()
{
    Console.Write("masukkan nilai lilin : ");
    string[] panjangLilin = Console.ReadLine().Trim().Split(" ");
    int[] panjangLilinInt = Array.ConvertAll(panjangLilin, int.Parse);
    int[] fibonacci = new int[panjangLilinInt.Length];
    fibonacci[0] = 0;
    fibonacci[1] = 1;

    for (int i = 0; i < fibonacci.Length; i++)
    {
        if (i >= 2)
        {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

    }

    bool end = true;
    do
    {
        for (int i = 0; i < panjangLilinInt.Length; i++)
        {
            panjangLilinInt[i] -= fibonacci[i];
            if (panjangLilinInt[i] <= 0)
            {
                end = false;
                Console.WriteLine($"Lilin yang habis duluan adalah lilin dengan index ke- {i}");
            }
        }
    } while (end);
}
